# Émission en bande de base

## I. Vitesse d'une transmission numérique

Dans le cadre d'une transmission numérique, l'émetteur (ici un capteur) envoie des données numériques au récepteur (ici un micro-contrôleur) via un canal sous la forme d'un signal cadencé:

![Communication capteur-uC](img/01-communication_capteur_uC.svg){ width=100% }

- Le signal est analogique
- Le cadencement est assuré par une horloge (transmise ou implicite) de période $T_H$.
- Émetteur, récepteur: capteur, actionneur, micro-contrôleur, ordinateur&hellip;
- Canal de transmission: piste de circuit imprimé, câbles (coaxial, paire torsadée, fibre), air&hellip;

Chaîne d'émission complète:

![Chaîne de transmission](img/01-chaine_transmission.svg)

- Codage de source: réduction de la taille des données à transmettre en enlevant les *redondances* (par compression).
- Codage de canal: ajout d'information *redondante* pour limiter les erreurs de transmissions.
- Modulation: décalage vers les hautes fréquences pour une transmission sans fil ou un multiplexage fréquentiel.

??? question "Exercice"

    {{ rg_dnd('Associer chaque terme à sa définition', [("codage de canal", "ajout d'informations redondantes"), ("codage de source", "suppression d'information redondante"), ("modulation", "décalage haute fréquence du signal")]) }}

### 1. Débit binaire

*[Débit binaire]: noté *D*

Il s'agit du nombre de bits issus de la source par unité de temps. Il s'exprime en **bits par seconde** (*bit/s* ou *bps*) et correspond à la fréquence bit $f_b$ (en *Hz*).

$$D = f_b = \dfrac{1}{T_b} \qquad\text{($T_b$: durée d'un bit)}$$

??? question "Exercice"

    === "Q1"
    
        {{ rg_qcm("Quelle est la durée d'un bit pour une transmission binaire à 10 000 bit/s ?", ["- 10 kHz", "+ 100 µs", "- 1 ms"]) }}

    === "Q2"

        {{ rg_qcm("Combien d'octets sont transmis par seconde lors d'une transmission binaire à 100 Mbps ?", ["- 100 000 000", "- 100 000", "+ 12 500 000"]) }}

### 2. Codage M-aire

#### a. Symboles

L'horloge cadence la transmission des **symboles** (= groupes de *n* bits) :

![Codage M-aire](img/01-codage_Maire.svg){ width=100% }

??? note "Remarque"

    Plus *n* augmente et plus la transmission est rapide (pour une même période d'horloge $T_H$)

    | n      | Durée de transmission | Symboles possibles       | Nom du codage  |
    |--------|-----------------------|--------------------------|----------------|
    | 1 bit  | $24 T_H$              | 0, 1                     | Codage binaire |
    | 2 bits | $12 T_H$              | 00, 01, 10, 11           | Codage 4-aire  |
    | 3 bits | $8 T_H$               | 000, 001&hellip; 111     | Codage 8-aire  |
    | 4 bits | $6 T_H$               | 00000, 0001&hellip; 1111 | Codage 16-aire |    

Pour des symboles de *n* bits, il existe $M = 2^n$ symboles distincts. Dans le cas général, on parle de codage **M-aire**.

??? question "Exercice"

    === "Q1"

        {{ rg_qcm("Comment appelle-t-on le codage pour des symboles de 5 bits ?", ["- 5-aire", "+ 32 aire", "- binaire"]) }}

    === "Q2"

        {{ rg_qcm("Combien de symboles distincts existe-t-il pour un codage 256-aire ?", ["+ 256", "- 8", "- 2"]) }}

    === "Q3"

        {{ rg_qcm("De combien augmente-t-on le débit lorsqu'on passe d'un codage binaire à un codage 8-aire ?", ["+ 3 fois", "- 8 fois", "- 2 fois"]) }}

#### b. Vitesse de transmission

*[Vitesse de transmission]: *débit symbole*, noté *R*

??? danger "Attention"

    Ne pas utiliser le terme: **rapidité de modulation**

C'est le nombre de symboles transmis par unité de temps. Elle s'exprime en **bauds** et correspond à la fréquence symbole $f_s$ (Hz).

$$ R = f_s = \dfrac{1}{T_s} \qquad\text{($T_s$: durée d'émission d'un symbole)}$$

### 3. Relation entre débit binaire et vitesse de transmission

$$ D = n\cdot R = \log_2(M) \cdot R$$

??? note "Remarque"

    Pour une transmission binaire, on a $D= R$.

??? question "Exercice"

    === "Q1"

        {{ rg_qcm("Que vaut le débit binaire d'une transmission à 10 000 bauds utilisant un codage 16-aire ?", ["- 160 kbps", "+ 40 kbps", "- 10 kbps", "- 2.5 kbps", "- 625 bps"]) }}

    === "Q2"

        {{ rg_qcm("Que vaut la vitesse pour une transmission de débit 10 000 bots utilisant un codage 2-aire ?", ["+ 10 000 bauds", "- 20 000 bauds", "- 5 000 bauds"]) }}

## II. PAM - Chronogrammes

Dans le cas d'une transmission en bande de base, et en ne considérant pas les codages de source et de canal, on a la chaîne de transmission suivante :

![Chaine bande de base](img/01-chaine_bdb.svg)

### 1. Principe de la PAM

*[PAM]: Pulse-Amplitude Modulation (modulation d'impulsions en amplitude)

Les amplitudes en sortie du codeur M-aire vont servir à modifier les amplitudes d'un train d'impulsions périodiques (période $T_H$) :

![Train d'impulsions](img/01-train_impulsions.svg)

??? note "Remarque"

    Le train est complètement défini par la donnée d'un seule impulsions (=le *motif*)

    ![Impulsion](img/01-impulsion.svg)

### 2. Exemples d'impulsions

#### a. Impulsion théorique de type NRZ

*[NRZ]: Non Retour à Zéro

Impulsion normalisée à une énergie de 1 J :

![Impulsion NRZ](img/01-impulsion_NRZ.svg)

!!! example "Exemple de PAM-2"

    Avec $a_0=-A$, $a_1=+A$

    ![Chronogramme NRZ](img/01-chronogramme_NRZ.svg){ width=100% }

Utilisations: I2C, RS232&hellip;

Autre exemple: PAM-4 pour transmettre 10110000

??? question "Exercice"

    Décoder les bits correspondant à cette transmission ($a_0=+A$, $a_1=-1$) :

    ![Chronogramme NRZ](img/01-chronogramme_NRZ-exo.svg){ width=100% }

    Bits = {{ rg_qsa("0010101") }}

#### b. Impulsion théorique de type RZ50

*[RZ50]: Retour à Zéro à 50% de Ts

Impulsion normalisée à 1 J :

![Impulsion RZ50](img/01-impulsion_RZ50.svg)

!!! example "Exemple de PAM-2"

    avec $a_0=-A$ et $a_1=+A$

    ![Chronogramme RZ50](img/01-chronogramme_RZ50.svg){ width=100% }

??? question "Exercice"

    Décoder les bits correspondant à cette transmission ($a_0=+A$, $a_1=-1$) :

    ![Chronogramme NRZ](img/01-chronogramme_RZ50-exo.svg){ width=100% }

    Bits = {{ rg_qsa("0011011") }}

#### c. Impulsion théorique de type Manchester 

*[Manchester]: ou biphase cohérent

Impulsion normalisée à 1 J :

![Impulsion RZ50](img/01-impulsion_Manchester.svg)

!!! example "Exemple de PAM-2"

    avec $a_0=-A$ et $a_1=+A$

    ![Chronogramme Manchester](img/01-chronogramme_Manchester.svg){ width=100% }

??? question "Exercice"

    Décoder les bits correspondant à cette transmission ($a_0=+A$, $a_1=-1$) :

    ![Chronogramme Manchester](img/01-chronogramme_Manchester-exo.svg){ width=100% }

    Bits = {{ rg_qsa("0100111") }}

## III. PAM - Comparaisons

### 1. Densité Spectrale de Puissance (DSP)

*[DSP]: Densité Spectrale de Puissance

Elle indique la répartition probabiliste de la puissance du signal en fonction de fréquences (i.e. enveloppe des spectres des signaux réels). Elle dépend de la forme de l'impulsion et de la fréquence symbole ($f_s$) choisies.

![DSP](img/01-DSP.svg){ width=100% }

On admet que l'occupation spectrale *finale* correspond à la largeur du premier lobe, c'est à dire:

- [0:$f_s$] pour l'impulsion NRZ,
- [0:$1.8f_s$] pour l'impulsion Manchester,
- [0:$2f_s$] pour l'impulsion RZ50.

??? question "Exercice"

    1. {{ rg_qcm("L'occupation spectrale maximale correspond à l'impulsion ?", ["- Manchester", "+ RZ50", "- NRZ"]) }}

    2. {{ rg_qcm("L'occupation spectrale minimale correspond à l'impulsion ?", ["- Manchester", "- RZ50", "+ NRZ"]) }}

### 2. Codages binaires unipolaires et bipolaires

=== "Bipolaire"

    - 2 niveaux de tension (ex: *+A* et *-A*). 
    - Une tension de 0 V correspond à un défaut sur la ligne
    - $U_{moy}\simeq 0$: utile pour des transmissions sur de longues distances.

=== "Unipolaire"

    - 1 niveau de tension (et le  0V). 
    - Une tension de 0 V correspond soit à un défaut, soit au repos.
    - $U_{moy}\simeq \frac{A}{2}$: uniquement pour des transmissions sur de courtes distances (à cause des phénomènes capacitifs).

![Codages unipolaire et bipolaire](img/01-unipolaire_bipolaire.svg){ width=100% }

??? question "Exercice"

    {{ rg_dnd("Associer la bonne caractéristique à chaque transmission:", [( "RS232", "codage bipolaire" ), ( "UART", "codage unipolaire" )]) }}

### 3. Transmission de l'horloge 

- NRZ: $DSP(f_s)=0$, l'horloge n'est pas transmise avec le signal.
- RZ50: unipolaire = bipolaire + horloge

    ![Horloge RZ50](img/01-horloge_RZ50.svg){ width=100% }

- Manchester: Manchester = NZR xor horloge

    ![Horloge Manchester](img/01-horloge_Manchester.svg){ width=100% }

??? question "Exercice"

    La DSP d'une transmission est la suivante : 

    ![DSP Manchester](img/01-DSP_Manchester.svg){ width=100% }

    {{ rg_qcm( "[horiz]L'horloge peut-elle être transmise dans le signal ?", ["+ oui", "- non"]) }}

### 4. Forme des impulsions

Puissances cumulatives:

![Puissances cumulatives](img/01-puissances_cumulatives.svg){ width=100% }

Comme l'essentiel de la puissance est contenue dans le premier lobe, il est possible de restreindre l'occupation spectrale:

- par filtrage passe-bas ou
- en utilisant directement des formes d'impulsions non rectangulaires.

On parle alors de *conditionnement du signal*.

![NRZ conditionné](img/01-NRZ_none.svg)

Spectres (à R=1 000 bauds)

| Rectangulaire | Cosinus surélevé |
|:-------------:|:----------------:|
| ![](img/01-spectre_NRZ_none.png) | ![](img/01-spectre_NRZ_raise_cosine.png)

{{ rg_corriger_page() }}