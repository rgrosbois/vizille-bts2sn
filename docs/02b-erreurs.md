# Détection et correction d'erreurs

L'opérateur `ou-exclusif` est noté $\oplus$ dans ce document.

??? info "Rappels sur le ou-exclusif"


    1. Rappeler sa table de vérité et son symbole normalisé dans un schéma.
    2. Que vaut $b_0 \oplus b1$ lorsque : 

        - les 2 bits sont égaux ?
        - les 2 bits sont différents ?

        Conclure sur une première utilité de cet opérateur.

    3. Exprimer $b \oplus 1$ en fonction de $b$.

        Que donne l'opération $m \oplus 0b11111111$ (avec *m*: mot binaire sur 8 bits) ?

        Conclure sur une deuxième utilité de cet opérateur.

    4. Que vaut $b_0\oplus b_1\oplus b_2\ldots\oplus b_7$ pour:

        - un nombre impair de bits à `1` ?
        - un nombre pair de bits à `1` ?

        Conclure sur une troisième utilité de cet opérateur.

    5. Donner les résultats des opérations binaires *101101 + 110011* et *101101 - 110011*.

        À quels types d'addition et soustraction correspond l'opération $1101101\oplus 110011$ ?

        Conclure sur une quatrième utilité de cet opérateur.

## 1. Bit de parité

![Bit de parité](img/src/02-bit_parite.svg){ width=65% }

Il s'agit d'un bit supplémentaire ajouté à la fin de chaque octet de manière à détecter une erreur de transmission dans ce dernier[^1].

[^1]: Cette méthode permet de détecter un nombre impair d'erreurs dans (octet + bit parité). 

Deux conventions sont possibles:

- parité paire: nombre {{ rg_qsa('pair') }} de 1 dans l'ensemble (octet + bit parité).
- parité impaire: nombre {{ rg_qsa('impair') }} de 1 dans l'ensemble (octet + bit parité).

1. Calculer les bits de parité impaires pour les octets dont les valeurs décimales sont les suivantes (écrire les octets au format petit-boutisme).

    ![Exercice bit de parité](img/src/02-exo_bit_parite.svg){ width=75% }

2. On considère la transmission d'un signal manchester (`1`: front montant) de débit binaire *D=10 Mbps*.

    1. Compléter le chronogramme  théorique dans le cas de l'utilisation d'un bit de parité paire et indiquer la valeur des octets transmis (le signal est au format *LSB-first*).

        ![10BASET parité paire](img/src/02-10BASET_parite_paire.svg){ width=95% }

    2. La transmission suivante utilise une parité impaire. 

        Indiquer si les octets transmis comportent des erreurs ou non (l'instant 21 &mu;s correspond au début d'un octet, les front à considérer sont situés sur chaque division verticale)

        ![10BASET parité impaire](img/src/02-10BASET_parite_impaire.svg){ width=95% }

    3. Comment se comporte ce système de détection si 2 erreurs de transmission surviennent dans un même ensemble (octet + bit de parité) ?

## 2. Somme de contrôle (checksum)

![Checksum](img/src/02-checksum.svg){ width=75% }

La détection s'obtient à l'aide d'un octet (*checksum*) placé à la suite de la séquence d'octets à contrôler. La *somme de contrôle* est obtenue par *addition sans propagation de la retenue* (ou-exclusif) des octets de la séquence.

1. Compléter l'opération d'addition sans retenue des 2 nombres décimaux `173` et `204`.

    ![Addition sans retenue](img/src/02-addition_sans_retenue.svg){ width=25% }

2. Calculer la somme de contrôle de la séquence d'octets : `18`, `125`, `206` et `34`.

    ![4 additions sans retenue](img/src/02-4additions_sans_retenue.svg){ width=25% }

    Dessiner le signal bande de base NRZ correspondant (`1`: état bas, octets au format petit-boutisme, $T_b$ correspond à 1 division)

    ![Checksum NRZ](img/src/02-checksum_NRZ.svg){ width=95% }

3. À quel calcul de parité particulier correspond cette somme de contrôle ?

## 3. Détecteur CRC-q bits

*[CRC]: Cyclic Redundancy Check (contrôle de redondance cyclique)

??? tip "FCS dans norme Ethernet"

    Le FCS à la fin d'une trame Ethernet est un CRC-32 bits[^2] qui permet de détecter les erreurs suivantes:

    - 1 seul bit erroné,
    - 2 bits erronés,
    - un nombre impair de bits erronés,
    - plusieurs bits erronés consécutifs (*burst*),
    - &hellip;

    *[FCS]: Frame Check Sequence
    [^2]: Diviseur: 0b1 0000 0100 1100 0001 0001 1101 1011 0111 = 0x104c11db7

Le CRC-q bits d'une séquence de *k* bits s'obtient en:

1. Ajoutant *q* bits à 0 à la fin de cette séquence pour obtenir le **dividende**
2. Divisant[^3] ce dividende par un **diviseur** (prédéfini) **de *q+1* bits**.
3. Ne conservant que le **reste** de la division (sur *q* bits)

!!! example "Exemple du CRC-5"

    Calculer le CRC-5 bits de la séquence 1001010100111 (le diviseur est 0b100101):

    ![Division CRC](img/src/02-division_CRC.svg){ width=95% }

??? question "Exercice"

    1. Calculer le CRC-3 bits (diviseur: 1011) de la séquence représentant le texte «123» en ASCII (au format *LSB-first*).

    2. Les 2 séquences suivantes se terminent par un CRC-4 (diviseur: 10110). Indiquer celle qui est correcte et identifier son message ASCII (au format *LSB-first*).

        - 0100001000101010110010101010
        - 0101001010101010111010111010

    3. Dans le cas où il n'y a pas d'erreur de transmission, que constate-t-on lorsqu'on divise directement la séquence suivie de son CRC ?

[^3]: il s'agit d'une division entière modulo 2 (utilisation du ou-exclusif).

## 4. Hamming (7,4)

Cette codage de canal permet de **corriger** au maximum 1 erreur par tranche de 7 bits (dont 4 de données).

### a. Codage

Il faut insérer 3 nouveaux bits de correction/détection ($c_2$, $c_1$, $c_0$) dans chaque quartet ($d_3$, $d_2$, $d_1$, $d_0$) à transmettre :

![Code Hamming 7,4](img/src/02-codage_hamming74.svg){ width=80% }

Calcul des bits de correction (utilisation du *ou-exclusif*) : $\begin{cases}c_2=d_3\oplus d_2\oplus d_1\\c_1=d_3\oplus d_1\oplus d_0\\c_0=d_3\oplus d_1\oplus d_0\end{cases}$

??? tip "Autre méthode"

    On montre que le code s'obtient à l'aide du produit matriciel[^4]:
  
    [^4]: le bloc `Hamming` de *LabVIEW Modulation Toolkit* utilise la matrice génératrice $\begin{pmatrix}1 & 1 & 0 & 1 & 0 & 0 & 0\\0 & 1 & 1 & 0 & 1 & 0 & 0\\1 & 1 & 1 & 0 & 0 & 1 & 0\\1 & 0 & 1 & 0 & 0 & 0 & 1\\\end{pmatrix}$.

    $$\begin{pmatrix} d_3 & d_2 & d_1 & c_2 & d_0 & c_1 & c_0 \end{pmatrix}
    = \begin{pmatrix}d_3 & d_2 & d_1 & d_0 \end{pmatrix}
    \cdot
    \begin{pmatrix}
      1 & 0 & 0 & 1 & 0 & 1 & 1\\
      0 & 1 & 0 & 1 & 0 & 1 & 0\\
      0 & 0 & 1 & 1 & 0 & 0 & 1\\
      0 & 0 & 0 & 0 & 1 & 1 & 1\\
    \end{pmatrix}$$

    L'intérêt du calcul matriciel est que cette équation se généralise pour la séquence entière de données:
    $$X = D \cdot \begin{pmatrix}
      1 & 0 & 0 & 1 & 0 & 1 & 1\\
      0 & 1 & 0 & 1 & 0 & 1 & 0\\
      0 & 0 & 1 & 1 & 0 & 0 & 1\\
      0 & 0 & 0 & 0 & 1 & 1 & 1\\\end{pmatrix}$$
    
    où la matrice *D* contient les quartets de données (1 par ligne) et la matrice *X* contient les codes à transmettre (1 par ligne).

??? question "Exercice"

    Quel est le flux de bits à transmettre pour le message ASCII «BTS» (en *LSB-first*) avec un codage Hamming (7,4) ?

### b. Décodage

Le décodage brut (i.e. sans correction) consiste à supprimer les bits de correction reçus. Pour un décodage avec correction, il faut :

1. Calculer la position de l'éventuelle erreur:

    ![Décodage Hamming 7,4](img/src/02-decodage_hamming74.svg){ width=75% }

    Calcul des bits de position: $\begin{cases}p_2 = d'_3 \oplus d'_2 \oplus d'_1 \oplus c'_2\\p_1 = d'_3 \oplus d'_2 \oplus d'_0 \oplus c'_1\\p_0 = d'_3 \oplus d'_1 \oplus d'_0 \oplus c'_0\\\end{cases}$

    ??? tip "Autre méthode"

        On montre que cette opération peut s'effectuer à l'aide du produit matriciel[^5]
        
        [^5]: le bloc `Hamming` de *LabVIEW Modulation Toolkit* utilise la matrice de validation $\begin{pmatrix}1 & 0 & 0\\0 & 1 & 0\\0 & 0 & 1\\1 & 1 & 0\\0 & 1 & 1\\1 & 1 & 1\\1 & 0 & 1\\\end{pmatrix}$

        $$ P = X' \cdot \begin{pmatrix}
        1 & 1 & 1\\
        1 & 1 & 0\\
        1 & 0 & 1\\
        1 & 0 & 0\\
        0 & 1 & 1\\
        0 & 1 & 0\\
        0 & 0 & 1\\\end{pmatrix}$$
    
        où $X'$ contient les codes reçus (avec potentiellement une erreur maximum par code)

2. Utiliser les bits de position pour consulter le tableau d'emplacement de l'éventuelle erreur:

    ![Tableau erreurs](img/src/02-tableau_erreurs.svg){ width=75% }

3. Corriger l'erreur (complément du bit identifié)

??? question "Questions"

    La réception d'un message ASCII utilisant le codage Hamming (7,4) donne la séquence 011000110110011101100010001101101000010011.

    1. Que donne le décodage *brut* de cette séquence ?

    2. Corriger les éventuelles erreurs et retrouver le message d'origine.

### c. Exercice

Décoder (en corrigeant) la séquence de transmission suivante:

`0010010011001000000101111100000100011010001101011111001
1010101101100011010001001000100100000001010111010001100
1010010010110111100010001000110111110111001110011011111
01011000100010001`

Caractéristique de cette séquence:

- 13 codes ASCII, soit 26 quartets dénommés `A1`, `A2`, `B1`&hellip;, `M2`.
- codage Hamming (7,4)
- format *LSB-first*.