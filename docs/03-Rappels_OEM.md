# Rappels sur les ondes électromagnétiques

## I. Onde électromagnétique en espace libre

L'onde est constituée de 2 champs $\vec{E}$ et $\vec{H}$ perpendiculaires entre eux:

- $\vec{E}$: champ électrique [V/m].
- $\vec{H}$: excitation magnétique [A/m].

??? info
    
    $\vec{H}=\dfrac{\vec{B}}{\mu}$ avec $\vec{B}$ champ magnétique en Tesla
    Dans l'air, $\mu=\mu_0=4\pi\times 10^{-7}~kg\cdot m\cdot A^{-2}\cdot s^{-2}$ 

$\vec{E}$ et $\vec{H}$ sont perpendiculaires à la direction de propagation, l'onde est qualifiée de **transverse**.

![Onde OEM](img/03-onde_OEM.svg){ width=80% }

Les ondes électromagnétiques peuvent être caractérisées par leur fréquence $f$ ou leur longueur d'onde $\lambda$ avec :

$$\lambda = \frac{v}{f}$$

??? tip "Précisions"

    $v$ est la vitesse de propagation de l'onde électromagnétique dans le milieu, $v=v_0\simeq 3\times 10^8~m/s$ dans le vide (ou l'air)

??? exercice

    La longueur d'une onde de fréquence *f=2.4 GHz* dans l'air vaut λ={{ rg_qnum(12.5, 0.1) }}cm.

    La fréquence d'une onde de longueur *λ=1.85 km* dans l'air vaut f={{ rg_qnum(162, 1) }}kHz

## II. Bandes de fréquences

Les basses fréquences, aussi appelées *ondes radio*, sont largement utilisées pour les communications

??? info

    Les avancées technologiques ont permis de générer des fréquences radios de plus en plus élevées, d'où l'utilisation *abusive* de superlatifs
    pour nommer des nouvelles bandes de fréquences (ultra, super, extra&hellip;).

Mises à part les communications lumineuses (ex: fibre), les bandes de plus hautes fréquences servent essentiellement à l'analyse ou sont des radiations émises spontanément par des corps astraux.


<object data="../img/03-bandes_OEM.svg"></object>

??? exercice

    1. Donner la signification des préfixes *P*, *T* et *E* dans les unités suivantes : 
    
        | Unité |  Signification                     |
        |-------|:-----------------------------------|
        | PHz   | P = {{ rg_qsa(["peta", "péta"]) }} |
        | THz   | T = {{ rg_qsa(["tera", "téra"]) }} |
        | EHz   | E = {{ rg_qsa(["exa"]) }}          |

    2. Que signifie le sigle *SHF* (en anglais, au pluriel) ?

        SHF = {{ rg_qsa(['super high frequencies']) }}

    3. Combien de décades séparent les bandes *VHF* et *EHF* ? 
    
        {{ rg_qnum(3, 0) }}décades

    4. On donne la liste des 6 canaux multiplex utilisés par l'émetteur de Chamrousse.

        ![Multiplex Chamrousse](img/03-Grenoble_Chamrousse.png)

        Donner le sigle de la bande de fréquence utilisée: {{ rg_qsa(['uhf']) }}
    
    5. {{ rg_qcm("Comment sont les longueurs d'ondes des infrarouges vis à vis de celles des ultraviolets ?", ["- plus petites", "+ plus grandes"]) }}

    6. {{ rg_qcm("Comment sont les fréquences des infrarouges vis à vis de celles des ultraviolets ?", ["+ plus petites", "- plus grandes"]) }}

    7. Quel est le sigle de la bande ultraviolet la plus éloignée de la lumière visible ? {{ rg_qsa(['euv']) }}

??? tip "Bandes ISM"

    Une telle bande peut être librement utilisée (dans certaines limites) sans demande d'autorisation.

    En Europe (tableau 1 de l'EN 55011) :

    | Décade | Valeurs                                            |
    |:---:|-------------------------------------------------------| 
    | HF  | 6,765 -   6,795 MHz (soit   6,78  MHz ±  15,0 kHz)    |
    | HF  | 13,553 -  13,567 MHz  (soit  13,56  MHz ±   7,0 kHz)  |
    | HF  | 26,957 -  27,283 MHz  (soit  27,12  MHz ± 163,0 kHz)  |
    | VHF | 40,660 -  40,700 MHz  (soit  40,68  MHz ±  20,0 kHz)  |
    | UHF | 433,05  - 434,79  MHz  (soit 433,920 MHz ± 0,2 %)     |
    | UHF |  2,4   -   2,5   GHz  (soit   2,450 GHz ±  50,0 MHz)  |
    | SHF |  5,725 -   5,875 GHz  (soit   5,800 GHz ±  75,0 MHz)  |
    | SHF | 24,0   -  24,25  GHz  (soit  24,125 GHz ± 125,0 MHz)  |
    | EHF | 61,0   -  61,5   GHz  (soit  61,25  GHz ± 250,0 MHz)  |
    | EHF | 122,0   - 123,0   GHz  (soit 122,50  GHz ± 500,0 MHz) |
    | EHF | 244,0   - 246,0   GHz  (soit 245,00  GHz ±   1,0 GHz) |


*[ISM]: (bande de fréquence) Industrielle, Scientifique et Médicale

## III. Puissances et décibels

Les puissances mises en jeu dans une transmission varient dans de larges proportions, selon qu'on se place du côté de l'émetteur ou du récepteur, il est donc souvent préférable de les exprimer en décibels.

On rappelle les définitions suivantes :

- L'expression en *dBW* (*dB-Watt*) d'une puissance *P* (en *W*) :
    
    $$P_{dBW} = 10\log\left(\dfrac{P}{1~W}\right)$$

- L'expression en *dBm* (*dB-milliwatt*) d'une puissance *P* (en *W*) :
    
    $$P_{dBm} = 10\log\left(\dfrac{P}{1~mW}\right)$$

??? info "Rappel"

    Le gain en puissance d'un amplificateur (d'amplification *A*): $G=10\log(A)=10\log\left(\dfrac{P_{\text{sortie}}}{P_{\text{entree}}}\right)$

??? exercice

    1. Exprimer *P=2 W* en *dBW*. 
    
        \(P_{dBW} =\) {{ rg_qnum(3, 0.1) }} dBW

    2. Exprimer *P=2 W* en *dBm*. 
    
        \(P_{dBm} =\) {{ rg_qnum(33, 0.2) }} dBm

    3. Exprimer *10 dBm* en *W*. 
    
        \(P=\) {{ rg_qnum(10, 0.2) }} mW


{{ rg_corriger_page('Corriger tous les exercices') }}