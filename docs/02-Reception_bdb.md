# Réception de signaux en bande de base

## I. Généralités

### 1. Principe

À chaque période symbole $T_S$, le récepteur prélève un échantillon du signal et identifie par seuillage le symbole correspondant :

![Réception bande de base idéale](img/src/02-reception_bdb_ideale.svg){ width=75% }

Exemple de seuillage binaire :

$$ d_k = \begin{cases}0 \text{, si $s(kT_S)<0$}\\1\text{, si $s(kT_S)\ge 0$}\end{cases}$$

!!! example "Mise en &oelig;uvre de récepteurs"

    === "NRZ binaire"

        Échantillonnage aux instants $t_p \equiv \frac{T_S}{2} \quad[T_S]$ :

        ![Signal bdb NRZ](img/src/02-signal_bdb_nrz.svg)
        

    === "RZ50 binaire"

        Échantillonnage aux instants $t_p\equiv\frac{T_S}{4}\quad [T_S]$ :
        
        ![Signal bdb RZ50](img/src/02-signal_bdb_rz50.svg)

    === "Manchester binaire"

        2 échantillons par période, aux instants $t_{p1}\equiv\frac{T_S}{4}\quad [T_S]$ et $t_{p2}\equiv\frac{3T_S}{4}\quad [T_S]$ :

        ![Signal bdb Manchester](img/src/02-signal_bdb_manchester.svg)

### 2. BER

*[BER]: Bit Error Rate

Plusieurs facteurs peuvent perturber le bon fonctionnement du récepteur et entraîner des erreurs de transmission. 

On mesure la qualité de la réception avec le *BER*[^1]:

[^1]: *TEB* (Taux d'Erreur Binaire) en français

$$ BER  = \frac{\text{Nombre de bits erronnés}}{\text{Nombre bits transmis}} $$

### 3. Diagramme de l'&oelig;il 

Ce graphe est obtenu par synchronisation de la base de temps sur l'horloge symbole (durée $2T_S$, ou $3T_S$&hellip;)[^2][^3]:

![Eye diagram](img/02-eye_diagram.svg)

La superposition des traces successives forme une figure appelée *diagramme de l'&oelig;il* et permet de déterminer le **seuil et l'instant d'échantillonnage optimaux**.

[^2]: Pour obtenir le le diagramme de l'&oelig;il sous LTSpice, il faut ajouter `.options baudrate=400k` (ou autre vitesse de transmission).
[^3]: Pour obtenir le diagramme de l'&oelig;il avec un oscilloscope:

    - Signal d'horloge (avec la fréquence symbole) sur l'entrée `EXT`
    - Signal de données sur l'entrée `CH1`.
    - Synchronisation de l'affichage sur `EXT`
    - Mode d'affichage avec persistence des courbes (`glitch`)

## II. Horloge de réception

### 1. Horloge symbole synchronisée

Le récepteur a besoin de régénérer l'horloge symbole de période $T_S$ pour échantillonner correctement le signal bande de base[^4].

![Horloge à 70%](img/src/02-recup_horloge70.svg)


[^4]: à un certain pourcentage de la période symbole (comptabilisé par exemple depuis un front montant). L'instant optimal peut être déterminé à l'aide du diagramme de l'&oelig;il.

### 2. Désynchronisation

Une mauvaise synchronisation entre les horloges d'émission et de réception peut entraîner des erreurs de décodage:

![Désynchronisation de l'horloge](img/src/02-desynchro_horloge.svg)

Ici aucun échantillon n'est prélevé pendant la 7ème période symbole (entre 6 ms et 7 ms).

### 3. Récupération de l'horloge

Cette opération est plus ou moins facile selon que le signal bande de base contient dans l'horloge (ex: Manchester, NRZ) ou non (NRZ) dans son spectre.

Diverses méthodes sont utilisées en pratique :

- extraction de l'horloge par filtrage passe-bande du signal reçu (lorsque l'horloge est contenue dans le spectre),
- ligne dédiée pour transmettre l'horloge de l'émetteur (ex: liaison I²C&hellip;): on parle alors de transmission **synchrone**,

    ![Trame I2C](img/02-Trame_I2C.png){ width=75% }

- resynchronisation de l'horloge récepteur sur les fronts du signal reçu (Cf TP),
- synchronisation continue de l'horloge grâce à une boucle à verrouillage de phase (*PLL*, Cf un prochain cours),
- &hellip;

### 4. Utilisation d'un préambule

Comme la synchronisation de l'horloge prend un certain temps, les trames de transmissions asynchrones[^5] débutent souvent par un préambule (c.à.d. des données *connues* durant le temps nécessaire à la synchronisation).

![Préambule USB](img/02-usb_preambule.png){ width=75% }

[^5]: Ethernet, USB&hellip;

## III. Défauts du canal

### 1. Bande-passante limitée

En première approximation, tout canal se comporte comme un filtre passe-bas de bande-passante *B*.

![Canal passe-bas](img/src/02-canal_passebas.svg)

??? tip "Rappel sur les fréquences harmoniques d'un signal créneau"

    ![Signal créneau](img/src/02-creneau.svg)

    Les harmoniques (toujours impairs) sont les premières fréquences atténuées par la bande-passante du canal.

=== "Bande-passante suffisante"

    ![Chronogramme canal](img/src/02-chronogramme_canal.svg)

    La transmission d'un symbole est légèrement perturbée par celle des symboles précédents: c'est l'**IES**.

    *[IES]: Interférence Entre Symboles

    ![IES sur le diagramme de l'&oelig;il](img/02-eye_IES_B.png)

=== "Bande-passante insuffisante"

    ![Chronogramme canal](img/src/02-chronogramme_canal2.svg)

    Ici l'IES est trop importante.

    ![IES trop importante diagramme de l'&oelig;il](img/02-eye_IES_0_25B.png)

Certaines mise en forme de codes ont pour objectif de limiter l'IES (ex: *cosinus surélevé*).

### 2. Bruit additif

![Canal bruité](img/src/02-canal_bruite.svg)

Le signal *bruité* reçu en sortie du canal peut être modélisé de la manière suivante:

![Modélisation du bruit](img/src/02-modelisation_bruit.svg)

où *s(t)*: signal émis (*parfait*) et *b(t)*: bruit additif dans le canal.

**Rapport signal sur bruit**

L'importance du bruit par rapport au signal se mesure à l'aide du SNR, généralement exprimé en décibels:

*[SNR]: Signal Noise Ratio (rapport signal sur bruit)

$$ SNR_{dB} = 10\log(\left(\frac{S}{N_0}\right)$$

où *S* est l'énergie du signal durant une période symbole ($T_S$) et $N_0$ est le double de la densité spectrale du bruit.

!!! example "Exemple d'une transmission NRZ binaire"

    === "SNR 40 dB"

        ![chronogramme SNR 40 dB](img/src/02-chrono_SNR40dB.svg)

        L'amplitude du signal est légèrement bruitée mais ne provoque pas d'erreur de décodage.

        ![diagramme SNR 40 dB](img/02-eye_SNR40dB.png)

    === "SNR 20 dB"

        ![chronogramme SNR 20 dB](img/src/02-chrono_SNR20dB.svg)

        L'amplitude du signal dépasse parfois le seuil de décodage et peut provoquer quelques erreurs.

        ![diagramme SNR 20 dB](img/02-eye_SNR20dB.png)

    === "SNR 10 dB"

        ![chronogramme SNR 10 dB](img/src/02-chrono_SNR10dB.svg)

        On ne distingue quasiment plus les 2 niveaux de tension, le décodage direct est impossible.

        ![diagramme SNR 10 dB](img/02-eye_SNR10dB.png)

### 3. Effets combinés

=== "Bruit faible"

    $SNR=40 dB, B=0.75f_S$

    ![SNR 40, 0.75 fs](img/02-BP750-SNR40dB.png)

=== "Bruit fort"

    $SNR=15 dB, B=0.75f_S$

    ![SNR 15, 0.75 fs](img/02-BP750-SNR15dB.png)

    L'&oelig;il est fermé

## IV. Récepteur bande de base

### 1. Probabilité d'erreur

La probabilité d'erreurs de transmissions n'est jamais nulle, quel que soit le rapport signal sur bruit.

On montre que: 

$$ P_{eb} = \frac{1}{2} erfc(\sqrt{SNR})$$

où $SNR=\sqrt{\frac{A^2T_S}{N_0}}$ (*A*: amplitude des impulsions, $T_S$: période symbole) et **erfc**  est une fonction  strictement décroissante. D'où la courbe :

![Peb vs SNR](img/src/02-erreur_complementaire.svg)

*[erfc]: fonction erreur complémentaire

### 2. Filtre de réception

Ce filtre a pour objectif d'augmenter le rapport signal sur bruit aux instants d'échantillonnage.

Dans le cas de la transmission d'un code binaire NRZ bipolaire en présence de bruit gaussien, le filtre optimal est un *intégrateur* (ou *moyenneur*) remis à zéro à chaque début de période symbole. L'instant d'échantillonnage optimal se trouve à la fin de la période symbole:

![Filtre optimal](img/src/02-filtre_optimal.svg)

### 3. Version finale

![Récepteur final](img/src/02-recepteur_bdb_final.svg)
