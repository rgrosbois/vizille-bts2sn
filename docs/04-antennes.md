# Transmissions par antennes en espace libre

<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>

N'importe quel conducteur traversé par un courant variable rayonne un champ électromagnétique autour de lui. La géométrie et les dimensions de ce conducteur 

## I. Antenne isotrope

!!! definition "Définition"

    Il s'agit d'un conducteur **ponctuel** qui rayonne avec la même intensité dans toutes les directions

??? info "Rayonnement"

    === "3D"
        <model-viewer src="../img/rayonnement_isotropique.glb" alt="Rayonnement isotropique" auto-rotate camera-controls style="width: 600px; height: 600px;"></model-viewer>


    === "2D"

        | Plan horizontal | Plan vertical |
        |:---------------:|:-------------:|
        | ![Rayonnement isotrope](img/04-diag_rayonnement_isotrope.svg) | ![Rayonnement isotrope](img/04-diag_rayonnement_isotrope.svg) |

Une telle antenne n'existe pas en pratique (on ne pourrait pas l'alimenter) mais elle permet de caractériser toutes les autres antennes réelles.

### 1. Densité de puissance rayonnée

L'antenne isotrope a un rayonnement à symétrie sphérique : la puissance $P_e$ issue de l'alimentation se répartie uniformément sur la surface d'une sphère[^1].

[^1]: Une sphére de rayon $r$ a un volume de $\frac{4}{3}\pi r^3$ et une surface de $4\pi r^2$.

![Rayonnement sphérique](img/04-rayonnement_spherique.svg){ width=85% }

!!! definition "Définition"

    La densité de puissance $\Pi_{iso}$ rayonnée à une distance $d$ par une antenne isotrope alimentée par une puissance $P_e$ est:

    $$\Pi_{iso} = \dfrac{P_e}{4\pi d^2}\qquad\text{(en $W/m^2$)}$$

Il s'agit de la puissance reçue par unité de surface (=$1~m^2$)

??? tip "Précisions"

    Un récepteur de surface $S_r$, à la distance $d$ reçoit une puissance $P_r=\Pi_{iso}S_r=\dfrac{P_eS_r}{4\pi d^2}$.

??? question "Exercice"

    On considère un récepteur pouvant capter les signaux en provenance de 2 sources différentes.

    - une première antenne isotrope est située à $d_1=10~km$ et est alimentée par une puissance $P_1=80~kW$,
    - une seconde antenne isotrope est située à une distance $d_2=5~km$ et est alimentée par une puissance $P_2=40~kW$.

        1. Déterminer la densité de puissance rayonnée par chaque source

            $\Pi_{iso1}=${{ rg_qnum(64, 1) }}$\mu W/m^2$  
        
            $\Pi_{iso2}=${{ rg_qnum(127, 2) }}$\mu W/m^2$

        2. {{ rg_qcm('[horiz]Déduire le signal qui sera le mieux capté par le récepteur', ["+ antenne 2", "- antenne 1"]) }}

### 2. Champ électrique

??? tip "Précisions"
    La valeur efficace du champ électrique $\vec{E}_{iso}$ est reliée à la densité de puissance par la relation:

    $$ \Pi_{iso} = E_{iso}\cdot H_{iso} \overset{\text{(vide)}}{\simeq} \dfrac{E^2_{iso}}{377} $$

Expression du champ électrique en fonction de la distance $d$ et de la puissance $P_e$ :

$$E_{iso} \overset{\text{(vide)}}{\simeq} \dfrac{\sqrt{30\cdot P_e}}{d} $$

??? question "Exercice"

    On considère un récepteur pouvant capter les signaux en provenance de 2 sources différentes:

    - une première antenne isotrope est située à $d_1=10~km$ et est alimentée par une puissance $P_1=80~kW$,
    - une seconde antenne isotrope est située à une distance $d_2=5~km$ et est alimentée par une puissance $P_2=40~kW$.

    Déterminer l'intensité efficace des champs électriques relatifs à chaque source :

    $E_{iso1}=${{ rg_qnum(0.15, 0.02) }}$V/m$  
        
    $E_{iso2}=${{ rg_qnum(0.22, 0.01) }}$V/m$

### 3. Limites d'exposition

Les normes françaises relatives à l'exposition aux champs électromagnétiques s'inspirent des recommandations périodiques émises par la CIPRNI (organisation non gouvernementale reconnue par l'OMS).

*[CIPRNI]: Commission Internationale de Protection contre les Rayonnements Non Ionisants

Quelques limites fixées en avril 1998 :

| Fréquence | Valeur maximale |
|-----------|-----------------|
| 50 Hz     | $E_{iso,max}=5~kV/m$ (professionnels: $E_{iso,max}=10~kV/m$ ) |
| 900 MHz   | $\Pi_{iso,max}=4.5~W/m^2$ (professionnels: $\Pi_{iso,max}=22.5~W/m^2$ ) |
| 1800 MHz  | $\Pi_{iso,max}=9~W/m^2$ (professionnels: $\Pi_{iso,max}=45~W/m^2$ ) |
| 2.45 GHz  | $\Pi_{iso,max}=10~W/m^2$ |

## II. Antenne directive

Contrairement à une antenne isotrope, une antenne réelle ne rayonne pas de manière identique dans toutes les directions.

!!! example "Exemple : antenne filaire verticale"

    <model-viewer src="../img/rayonnement_conducteur.glb" alt="conducteur" auto-rotate camera-controls style="width: 600px; height: 600px;"></model-viewer>
            
### 1. Diagramme de rayonnement

Ce type de diagramme donne la répartition de la puissance rayonnée en fonction de la direction.

!!! example "Exemple: antenne filaire verticale"

    ![Diagramme rayonnement conducteur filaire](img/04-rayonnement_conducteur_vertical.svg){ width=100% }

??? question "Exercice"

    {{ rg_dnd("Identifier les plans horizontal et vertical sur les 2 diagrammes 2D précédents", 
    [ ('Plan (a)', 'horizontal'), ('Plan (b)', 'vertical' ) ]) }}

### 2. Angle d'ouverture à -3 dB

C'est l'angle compris entre les 2 directions où $P_{dB} \ge P_{dB,max} - 3 dB$.

L'antenne est dite :

- **directive** si elle privilégie une direction particulière.
- **omnidirectionnelle** si elle ne privilégie aucune direction (ex: antenne isotrope ou plan horizontal d'une antenne filaire).

Plus l'antenne est directive et plus l'angle d'ouverture est étroit.

??? question "Exercice"

    Que vaut l'angle d'ouverture de l'antenne ci-après ? {{ rg_qnum(36,5) }}°

    ![Ouverture d'une antenne directive](img/04-ouverture_antenne_directive.svg){ width=75% }

### 3. Gain/amplification de l'antenne

Le gain de puissance par rapport à l'antenne **isotrope** dans la direction principale de rayonnement vaut:

$$ G_{dBi} = P_{dB,max} - P_{dB,iso} $$

Par convention, il s'exprime en **dBi** (*i* pour isotrope).

L'amplification $A=\frac{P_{max}}{P_{iso}}$ est reliée au gain en puissance par la formule : $G_{dBi}=10\log(A)$.

??? question "Exercice"

    Une antenne a le diagramme de rayonnement suivant : 

    ![Gain d'une antenne directive](img/04-gain_antenne_directive.svg){ width=75% }

    1. Que vaut le gain de l'antenne ? G={{ rg_qnum(15,2) }}dBi

    2. Que vaut l'amplification de cette antenne ? A={{ rg_qnum(31.6, 0.5) }}

Plus l'antenne est directive et plus son gain est important.

## III. Bilan de liaison

On considère une antenne émettrice de gain $G_e$ (amplification $A_e$) alimentée par une source de puissance $P_e$. Une antenne réceptrice de gain $G_r$ (amplification $A_r$) située à une distance *d* récupère une puissance $P_r$ que l'on cherche à calculer.

![Bilan de liaison](img/04-bilan_liaison.svg){ width=75% }

### 1. PIRE

![Bilan de liaison](img/04-PIRE.svg){ width=75% }

On définit la **Puissance Isotrope Rayonnée Équivalente** (PIRE) d'une antenne par la relation:

$$PIRE = A_eP_e$$

où :

- $A_e$ est l'amplification de l'antenne.
- $P_e$ est la puissance d'émission (en *W*).

!!! note "Remarque"

    - En décibels, cette expression devient : $PIRE_{dBx} = G_{e,dBi} + P_{e,dBx}$.
    - La valeur maximale de la PIRE est réglementée dans certaines bandes de fréquences (ex: WiFi).

??? question "Exercice"

    Que vaut la PIRE d'une antenne 10 dBi alimentée par 15 W ?

    PIRE = {{ rg_qnum(150, 1) }}W

### 2. Grandeurs physiques transmises

En utilisant les résultats de l'antenne isotrope, on obtient les expressions de la densité de puissance et du champ électrique en fonction de la distance :

$$\begin{cases}
    \Pi = \dfrac{P_eA_e}{4\pi d^2} = \dfrac{PIRE}{4\pi d^2}\\
    E\simeq \dfrac{\sqrt{30\cdot PIRE}}{d}
\end{cases}$$

??? question "Exercice"

    Que valent la densité de puissance et le champ électrique efficace à 100 m d'une antenne de PIRE=100 W ?

    $\Pi$= {{rg_qnum(0.8, 0.1) }}mW/m²

    *E*={{ rg_qnum(0.5, 0.05) }}V/m

### 3. Puissance reçue

La surface *effective* de réception (en m²) est définie par $S_r=A_r\dfrac{\lambda^2}{4\pi}$.

La puissance reçue (en *W*) vaut donc :

$$ P_r = S_r\Pi = A_r\left(\dfrac{\lambda}{4\pi D}\right)^2PIRE
  = P_eA_eA_r\left(\dfrac{\lambda}{4\pi D}\right)^2 $$

Si on exprime cette relation en décibels (formule de **Fris**) : 

$$P_{r,dB} = P_{e,dB} + G_e + G_r + 20\log\left(\dfrac{\lambda}{4\pi}\right) - 20\log(D)$$

??? tip "Précision"

    Le terme $20\log(D)-20\log\left(\frac{\lambda}{4\pi}\right)$ correspond aux pertes de transmission (elles sont toujours supérieures en réalité).

## IV. Exemples d'antennes

![Ligne sans perte](img/04-ligne_sans_perte.svg){ align=left }
Bien souvent, une antenne est une ligne de transmission de longueur *L* : 

- d'impédance caractéristique : $Z_c\simeq 50~\Omega, 75~\Omega$&hellip; (adaptée à la ligne d'alimentation).

- d'impédance de sortie : infinie (ligne ouverte).


L'impédance d'entrée dépend de sa longueur *L* :

![Impédance d'une ligne ouverte](img/04-impedance_ligne_ouverte.svg){ width=75% }

Pour un rayonnement maximal, il faut $Z_{in}\rightarrow \infty$

### 1. Antenne dipôle

(ou *ruban*, *doublet*, *demi-onde*)

|    | | |
|----|:---:|:----:|
| ![Antenne doublet](img/04-antenne_doublet.svg) | ![Dipole FM](img/04-antenne_emetteur_FM.jpg) | ![Antenne dipole](img/04-antenne_dipole.jpg)

Il s'agit de l'antenne la plus courte selon le résultat précédent :

- $L=\dfrac{\lambda}{2}$
- raccordement de l'alimentation (*feeder*) en son centre ($\frac{\lambda}{4}\rightarrow$ impédance *nulle*).
- Le deuxième conducteur est généralement inutile (les 2 extrémités sont ouvertes).

Cette antenne est omnidirectionnelle dans son plan perpendiculaire (ici horizontal) et a un gain typique de 2.1 dBi[^2]

[^2]: L'antenne dipôle est parfois utilisée comme référence (à la place de l'antenne isotrope) pour les autres antennes. Les gains de ces dernières s'exprimes alors en **dBd** (où *d* signifie dipôle) avec *0 dBd = 2.1 dBi*.

??? info "Diagramme de rayonnement"

    === "3D"

        <model-viewer src="../img/rayonnement_dipole.glb" alt="dipole" auto-rotate camera-controls style="width: 600px; height: 600px;"></model-viewer>

    === "2D"

        | Plan vertical | Plan horizontal |
        |:---:|:----:|
        | ![Plan vertical](img/04-rayonnement_dipole_vertical.svg) | ![Plan horizontal](img/04-rayonnement_dipole_horizontal.svg) |

### 2. Antenne Yagi

(ou antenne *rateau*)

|    | | |
|----|:---:|:----:|
| ![Principe Yagi7](img/04-antenne_Yagi7.svg) | ![Antenne Yagi5](img/04-antenne_Yagi.jpg) | ![Antenne Yagi7](img/04-Yagi7.jpg)

Elle est constituée de :

- un brin dipôle rayonnant (demi-onde)
- un brin réflecteur (longueur $\ge \frac{\lambda}{2}$).
- un ou plusieurs brins directeurs (longueur $\le \frac{\lambda}{2}$).

??? info "Diagramme de rayonnement"

    | Plan vertical | Plan horizontal |
    |:---:|:----:|
    | ![Plan vertical](img/04-rayonnement_Yagi7-horizontal.svg) | ![Plan horizontal](img/04-rayonnement_Yagi7-vertical.svg) |

    G = 12 dBi

??? info "Influence des divers brins"

    === "1 brin"

        | G=2.1 dBi   | | |
        |----|:---:|:----:|
        | ![Yagi7-1](img/04-Yagi7-1.svg) | ![Rayonnement Yagi7-1 horizontal](img/04-rayonnement_Yagi7-1-horizontal.svg) | ![Rayonnement Yagi7-1 vertical](img/04-rayonnement_Yagi7-1-vertical.svg)

    === "2 brins"

        | G=6.7 dBi   | | |
        |----|:---:|:----:|
        | ![Yagi7-2](img/04-Yagi7-2.svg) | ![Rayonnement Yagi7-2 horizontal](img/04-rayonnement_Yagi7-2-horizontal.svg) | ![Rayonnement Yagi7-2 vertical](img/04-rayonnement_Yagi7-2-vertical.svg)

    === "3 brins"

        | G=9.1 dBi   | | |
        |----|:---:|:----:|
        | ![Yagi7-3](img/04-Yagi7-3.svg) | ![Rayonnement Yagi7-3 horizontal](img/04-rayonnement_Yagi7-3-horizontal.svg) | ![Rayonnement Yagi7-3 vertical](img/04-rayonnement_Yagi7-3-vertical.svg)

    === "4 brins"

        | G=9.7 dBi   | | |
        |----|:---:|:----:|
        | ![Yagi7-4](img/04-Yagi7-4.svg) | ![Rayonnement Yagi7-4 horizontal](img/04-rayonnement_Yagi7-4-horizontal.svg) | ![Rayonnement Yagi7-4 vertical](img/04-rayonnement_Yagi7-4-vertical.svg)

    === "5 brins"

        | G=11.0 dBi   | | |
        |----|:---:|:----:|
        | ![Yagi7-5](img/04-Yagi7-5.svg) | ![Rayonnement Yagi7-5 horizontal](img/04-rayonnement_Yagi7-5-horizontal.svg) | ![Rayonnement Yagi7-5 vertical](img/04-rayonnement_Yagi7-5-vertical.svg)

    === "6 brins"

        | G=11.2 dBi   | | |
        |----|:---:|:----:|
        | ![Yagi7-6](img/04-Yagi7-6.svg) | ![Rayonnement Yagi7-6 horizontal](img/04-rayonnement_Yagi7-6-horizontal.svg) | ![Rayonnement Yagi7-6 vertical](img/04-rayonnement_Yagi7-6-vertical.svg)

### 3. Antenne quart d'onde

(ou *fouet*, *Marconi*)

Cette antenne est placée sur un plan de masse (ou possède 3 brins de masse appelés, *radians*, de même longueur que le brin actif). La masse joue un rôle de *miroir* qui permet de diminuer la taille de l'antenne de moitié.

|    | | |
|----|:---:|:----:|
| ![Principe antenne fouet](img/04-antenne_fouet.svg) | ![Antenne fouet](img/04-antenne_fouet.jpg) | ![Antenne 1/4 onde brins](img/04-antenne_quart_onde_brins.jpg)

Le raccordement à l'amplificateur (émetteur ou récepteur) se fait au pied de l'antenne.

### 4. Antenne à réflecteur parabolique

L'onde se réfléchit sur la parabole et se concentre au foyer d'un voisinage duquel se trouve un *guide d'onde* circulaire :

|    | | 
|----|:---:|
| ![Antenne parabolique](img/04-antenne_parabolique.jpg) | ![Principe guide d'onde](img/04-guide_onde.svg) | 

??? info "Précisions"

    Le guide d'onde a une longueur d'onde de coupure $\lambda_c\simeq 1.706\cdot D$ et la longueur de l'onde $\lambda_g$ à l'intérieur est plus longue que celle dans l'air:

    $$\dfrac{1}{\lambda_g^2} = \dfrac{1}{\lambda^2} - \dfrac{1}{\lambda_c^2}$$
    
Cet type d'antenne est très directif et s'utilise pour les *petites* longueurs d'ondes.

