# Sciences Physiques en BTS 2 SN

- [Émission en bande de base](01-Emission_bdb.md)
- [Réception de signaux en bande de base](02-Reception_bdb.md)
    - TD: [détection/correction d'erreur](02b-erreurs.md)
- Rappel: [Ondes électromagnétiques](03-Rappels_OEM.md)
- [Transmissions en espace libre](04-antennes.md)
- Modulations
- Systèmes linéaires analogiques
- Systèmes numériques
- Image et couleurs