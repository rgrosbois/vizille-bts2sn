set table "02-DSP_Manchester.pgf-plot.table"; set format "%.5f"
set format "%.7e";; set samples 200; set dummy x; plot [x=1e-6:4]  (sin(pi/2*x))**4/((pi/2*x))**2;
