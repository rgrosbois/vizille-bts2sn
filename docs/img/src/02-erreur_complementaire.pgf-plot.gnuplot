set table "02-erreur_complementaire.pgf-plot.table"; set format "%.5f"
set format "%.7e";; set samples 25; set dummy x; set logscale y 2.71828182845905; plot [x=1:12] 0.5*erfc(sqrt(10**(x/10)));
